import { useState } from "react";
import "./App.css";

const BACK_END_URL = "http://localhost:4000";

function App() {
  const [msg, setMsg] = useState("");
  const [msgDate, setMsgDate] = useState("");

  const callAllo = async () => {
    const response = await fetch(BACK_END_URL + "/allo");
    const msgJson = await response.json();
    setMsg(msgJson.msg);
    setMsgDate(msgJson.date);
  };

  return (
    <>
      <h1>Appeler</h1>

      <button onClick={() => callAllo()} type="button">
        <h1>Allo</h1>
      </button>

      <h2>Msg : {msg}</h2>
      <h2>Msg Date : {msgDate}</h2>
    </>
  );
}

export default App;
